<?php

namespace FlightBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Aircraft
 *
 * @ORM\Table(name="un_travel_aircraft")
 * @ORM\Entity(repositoryClass="FlightBundle\Repository\AircraftRepository")
 * @UniqueEntity("name")
 */
class Aircraft
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $availability;

    /**
     *  @ORM\OneToMany(targetEntity="Flight", mappedBy="flightAircraft")
     */
    protected $flights;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flights = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Aircraft
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set availability
     *
     * @param string $availability
     *
     * @return Aircraft
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Add flight
     *
     * @param \FlightBundle\Entity\Flight $flight
     *
     * @return Aircraft
     */
    public function addFlight(\FlightBundle\Entity\Flight $flight)
    {
        $this->flights[] = $flight;

        return $this;
    }

    /**
     * Remove flight
     *
     * @param \FlightBundle\Entity\Flight $flight
     */
    public function removeFlight(\FlightBundle\Entity\Flight $flight)
    {
        $this->flights->removeElement($flight);
    }

    /**
     * Get flights
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFlights()
    {
        return $this->flights;
    }

    public function __toString()
    {
        return $this->getName();
    }
}

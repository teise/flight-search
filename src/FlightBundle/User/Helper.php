<?php

namespace FlightBundle\User;

use Doctrine\ORM\Mapping\Entity;

/**
 * Class Helper
 * @package FlightBundle\User
 */
class Helper {

    /**
     * @param null $result
     * @return null
     */
    public static function prepareAirport($result = null)
    {
        if (is_null($result))
            return null;

        foreach($result as $en) {
            $arResult[$en->getName()] = $en->getId();
        }

        return $arResult;
    }

    public static function setTravelTime($params = null)
    {

    }

} 
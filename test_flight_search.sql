CREATE DATABASE  IF NOT EXISTS `travel` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `travel`;
-- MySQL dump 10.13  Distrib 5.6.30, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: travel
-- ------------------------------------------------------
-- Server version	5.6.30-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `un_travel_aircraft`
--

DROP TABLE IF EXISTS `un_travel_aircraft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `un_travel_aircraft` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `availability` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_90803DAF5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `un_travel_aircraft`
--

LOCK TABLES `un_travel_aircraft` WRITE;
/*!40000 ALTER TABLE `un_travel_aircraft` DISABLE KEYS */;
INSERT INTO `un_travel_aircraft` VALUES (1,'AirBus','230');
/*!40000 ALTER TABLE `un_travel_aircraft` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `un_travel_airline`
--

DROP TABLE IF EXISTS `un_travel_airline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `un_travel_airline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_66CB5FEF5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `un_travel_airline`
--

LOCK TABLES `un_travel_airline` WRITE;
/*!40000 ALTER TABLE `un_travel_airline` DISABLE KEYS */;
INSERT INTO `un_travel_airline` VALUES (1,'AirFlot','Russia'),(2,'RedWings','Germany'),(3,'Luft Hanse','Germany'),(4,'USA Airline','USA');
/*!40000 ALTER TABLE `un_travel_airline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `un_travel_airport`
--

DROP TABLE IF EXISTS `un_travel_airport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `un_travel_airport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `time_zone` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F44EB6D55E237E06` (`name`),
  UNIQUE KEY `UNIQ_F44EB6D577153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `un_travel_airport`
--

LOCK TABLES `un_travel_airport` WRITE;
/*!40000 ALTER TABLE `un_travel_airport` DISABLE KEYS */;
INSERT INTO `un_travel_airport` VALUES (1,'Gette','Berlin','Germany','GTE',2),(2,'Domodedovo','Moscow','Russia','DMD',3);
/*!40000 ALTER TABLE `un_travel_airport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `un_travel_flight`
--

DROP TABLE IF EXISTS `un_travel_flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `un_travel_flight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `date_departure` datetime NOT NULL,
  `date_arrival` datetime NOT NULL,
  `availability` int(11) NOT NULL,
  `flight_number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `origin_airport_id` int(11) DEFAULT NULL,
  `destination_airport_id` int(11) DEFAULT NULL,
  `airline_id` int(11) DEFAULT NULL,
  `aircraft_id` int(11) DEFAULT NULL,
  `date_departure_ymd` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_78DF1D1591440985` (`origin_airport_id`),
  KEY `IDX_78DF1D1575A6C87A` (`destination_airport_id`),
  KEY `airline` (`airline_id`),
  KEY `availability` (`availability`),
  KEY `IDX_78DF1D15846E2F5C` (`aircraft_id`),
  KEY `date_origin_destination` (`date_departure_ymd`,`origin_airport_id`,`destination_airport_id`),
  CONSTRAINT `FK_78DF1D15130D0C16` FOREIGN KEY (`airline_id`) REFERENCES `un_travel_airline` (`id`),
  CONSTRAINT `FK_78DF1D1575A6C87A` FOREIGN KEY (`destination_airport_id`) REFERENCES `un_travel_airport` (`id`),
  CONSTRAINT `FK_78DF1D15846E2F5C` FOREIGN KEY (`aircraft_id`) REFERENCES `un_travel_aircraft` (`id`),
  CONSTRAINT `FK_78DF1D1591440985` FOREIGN KEY (`origin_airport_id`) REFERENCES `un_travel_airport` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `un_travel_flight`
--

LOCK TABLES `un_travel_flight` WRITE;
/*!40000 ALTER TABLE `un_travel_flight` DISABLE KEYS */;
INSERT INTO `un_travel_flight` VALUES (4,NULL,NULL,'2016-07-03 10:00:00','2016-07-04 02:00:00',123,'12245',10,1,2,1,1,20160703),(6,NULL,NULL,'2016-07-04 12:00:00','2016-07-04 16:00:00',120,'gdfgdf',20,1,2,1,1,20160704),(7,NULL,NULL,'2016-07-15 12:00:00','2016-07-15 13:00:00',120,'cvbcv',30,2,1,1,1,20160715),(8,NULL,NULL,'2011-01-01 00:00:00','2011-01-01 00:00:00',123,'12sdfsd',40,1,1,1,1,20110101),(9,NULL,NULL,'2016-07-04 23:19:00','2016-07-05 05:19:00',30,'qwe121',50,1,2,2,1,20160704),(10,NULL,NULL,'2016-07-04 23:20:00','2016-07-05 03:20:00',120,'asdas12',90,1,1,3,1,20160704),(11,NULL,NULL,'2016-07-04 23:20:00','2016-07-05 08:20:00',130,'asasd12',100,1,1,4,1,20160704),(12,NULL,NULL,'2016-07-04 09:21:00','2016-07-03 23:21:00',13,'asdasd',90,1,2,1,1,20160704),(13,NULL,NULL,'2016-07-04 10:21:00','2016-07-04 23:21:00',15,'afasd12',70,1,1,1,1,20160704),(14,NULL,NULL,'2016-07-10 07:24:00','2016-07-10 10:24:00',13,'asdasd',60,1,2,1,1,20160710),(15,NULL,NULL,'2016-07-10 05:24:00','2016-07-10 12:24:00',15,'asda13',90,1,1,1,1,20160710),(16,NULL,NULL,'2016-07-10 06:27:00','2016-07-10 09:27:00',13,'asdas13',60,2,1,1,1,20160710),(17,NULL,NULL,'2016-07-10 14:27:00','2016-07-10 18:27:00',13,'dasda12',90,1,1,2,1,20160710),(18,NULL,NULL,'2016-07-04 08:51:00','2016-07-04 08:51:00',130,'fsdfsdf',50,1,1,1,1,20160704);
/*!40000 ALTER TABLE `un_travel_flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `un_travel_flight_book`
--

DROP TABLE IF EXISTS `un_travel_flight_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `un_travel_flight_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create` datetime NOT NULL,
  `update` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `flight_id` int(11) NOT NULL,
  `passenger_number` int(11) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `un_travel_flight_book`
--

LOCK TABLES `un_travel_flight_book` WRITE;
/*!40000 ALTER TABLE `un_travel_flight_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `un_travel_flight_book` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-04  8:57:47

<?php

namespace FlightBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FlightBundle\Entity\Flight;
use FlightBundle\Form\FlightType;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


/**
 * Flight controller.
 *
 * @Route("/flight")
 */
class FlightController extends Controller
{
    /**
     * Lists all Flight entities.
     *
     * @Route("/", name="flight_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $flights = $em->getRepository('FlightBundle:Flight')->findAll();


        return $this->render('flight/index.html.twig', array(
            'flights' => $flights,
        ));
    }

    /**
     * Creates a new Flight entity.
     *
     * @Route("/new", name="flight_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $flight = new Flight();
        $options = [];

        $em = $this->getDoctrine()->getManager();

        $options['airports'] = \FlightBundle\User\Helper::prepareAirport(
                $em->getRepository('FlightBundle:Airport')->findAll());

        $options['aircrafts'] = \FlightBundle\User\Helper::prepareAirport(
            $em->getRepository('FlightBundle:Aircraft')->findAll());

        $options['airlines'] = \FlightBundle\User\Helper::prepareAirport(
            $em->getRepository('FlightBundle:Airline')->findAll());

        $form = $this->createForm('FlightBundle\Form\FlightManager', $flight, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $form->getData()->setOriginAirport(
                $em->getRepository('FlightBundle:Airport')
                    ->find($form->getData()->getOriginAirport())
            );

            $form->getData()->setDestinationAirport(
                $em->getRepository('FlightBundle:Airport')
                    ->find($form->getData()->getDestinationAirport())
            );

            $form->getData()->setFlightAirline(
                $em->getRepository('FlightBundle:Airline')
                    ->find($form->getData()->getFlightAirline())
            );

            $form->getData()->setFlightAircraft(
                $em->getRepository('FlightBundle:Aircraft')
                    ->find($form->getData()->getFlightAircraft())
            );

            $flight->setDateDepartureYmd($form->getData()->getDateDeparture()->format('Ymd'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($flight);
            $em->flush();

            return $this->redirectToRoute('flight_show', array('id' => $flight->getId()));
        }

        return $this->render('flight/new.html.twig', array(
            'flight' => $flight,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Flight entity.
     *
     * @Route("/{id}", name="flight_show")
     * @Method("GET")
     */
    public function showAction(Flight $flight)
    {

        $deleteForm = $this->createDeleteForm($flight);

        return $this->render('flight/show.html.twig', array(
            'flight' => $flight,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Flight entity.
     *
     * @Route("/{id}/edit", name="flight_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Flight $flight)
    {
        $deleteForm = $this->createDeleteForm($flight);

        $em = $this->getDoctrine()->getManager();

        $options['airports'] = \FlightBundle\User\Helper::prepareAirport(
            $em->getRepository('FlightBundle:Airport')->findAll());

        $options['aircrafts'] = \FlightBundle\User\Helper::prepareAirport(
            $em->getRepository('FlightBundle:Aircraft')->findAll());

        $options['airlines'] = \FlightBundle\User\Helper::prepareAirport(
            $em->getRepository('FlightBundle:Airline')->findAll());

        $options['origin_airport_id'] = $flight->getOriginAirport()->getId();
        $options['destination_airport_id'] = $flight->getDestinationAirport()->getId();
        $options['airline_id']  = $flight->getFlightAirline()->getId();
        $options['aircraft_id'] = $flight->getFlightAircraft()->getId();

        $editForm = $this->createForm('FlightBundle\Form\FlightManager', $flight, $options);


        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $editForm->getData()->setOriginAirport(
                $em->getRepository('FlightBundle:Airport')
                    ->find($editForm->getData()->getOriginAirport())
            );

            $editForm->getData()->setDestinationAirport(
                $em->getRepository('FlightBundle:Airport')
                    ->find($editForm->getData()->getDestinationAirport())
            );

            $editForm->getData()->setFlightAirline(
                $em->getRepository('FlightBundle:Airline')
                    ->find($editForm->getData()->getFlightAirline())
            );

            $editForm->getData()->setFlightAircraft(
                $em->getRepository('FlightBundle:Aircraft')
                    ->find($editForm->getData()->getFlightAircraft())
            );

            $em = $this->getDoctrine()->getManager();
            $em->persist($flight);
            $em->flush();

            return $this->redirectToRoute('flight_index', array('id' => $flight->getId()));
        }

        return $this->render('flight/edit.html.twig', array(
            'flight' => $flight,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Flight entity.
     *
     * @Route("/delete/{id}", name="flight_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Flight $flight)
    {

        if ($flight->getId() > 0) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($flight);
            $em->flush();
        }

        return $this->redirectToRoute('flight_index');
    }

    /**
     * Creates a form to delete a Flight entity.
     *
     * @param Flight $flight The Flight entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Flight $flight)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('flight_delete', array('id' => $flight->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}

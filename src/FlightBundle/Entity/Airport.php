<?php

namespace FlightBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Airport
 *
 * @ORM\Table(name="un_travel_airport")
 * @ORM\Entity(repositoryClass="FlightBundle\Repository\AirportRepository")
 * @UniqueEntity("name")
 * @UniqueEntity("code")
 */
class Airport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=5, unique=true)
     */
    protected $code;

    /**
     * @ORM\Column(type="integer", length=3)
     */
    protected $timeZone;

    /**
     *  @ORM\OneToMany(targetEntity="Flight", mappedBy="originAirport")
     */
    protected $origin;

    /**
     *  @ORM\OneToMany(targetEntity="Flight", mappedBy="destinationAirport")
     */
    protected $destination;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->origin = new \Doctrine\Common\Collections\ArrayCollection();
        $this->destination = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Airport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Airport
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Airport
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Airport
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set timeZone
     *
     * @param integer $timeZone
     *
     * @return Airport
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * Get timeZone
     *
     * @return integer
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * Add origin
     *
     * @param \FlightBundle\Entity\Flight $origin
     *
     * @return Airport
     */
    public function addOrigin(\FlightBundle\Entity\Flight $origin)
    {
        $this->origin[] = $origin;

        return $this;
    }

    /**
     * Remove origin
     *
     * @param \FlightBundle\Entity\Flight $origin
     */
    public function removeOrigin(\FlightBundle\Entity\Flight $origin)
    {
        $this->origin->removeElement($origin);
    }

    /**
     * Get origin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Add destination
     *
     * @param \FlightBundle\Entity\Flight $destination
     *
     * @return Airport
     */
    public function addDestination(\FlightBundle\Entity\Flight $destination)
    {
        $this->destination[] = $destination;

        return $this;
    }

    /**
     * Remove destination
     *
     * @param \FlightBundle\Entity\Flight $destination
     */
    public function removeDestination(\FlightBundle\Entity\Flight $destination)
    {
        $this->destination->removeElement($destination);
    }

    /**
     * Get destination
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     *  Get Airport Name
     *
     * @return string
     */
    public function getAirportName()
    {
        return $this->getCity() ." " . $this->getName() . "(" . $this->getCountry() . ")";
    }

    public function __toString()
    {
        return $this->getAirportName();
    }
}

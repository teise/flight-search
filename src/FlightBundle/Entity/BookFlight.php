<?php

namespace FlightBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BookFlight
 *
 * @ORM\Table(name="un_travel_flight_book")
 * @ORM\Entity(repositoryClass="FlightBundle\Repository\BookFlightRepository")
 */
class BookFlight
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    protected $create;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $update;

    /**
     * @ORM\Column(type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $flightId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $passengerNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $paid = false;



    /**
     * Set create
     *
     * @param \DateTime $create
     *
     * @return BookFlight
     */
    public function setCreate($create)
    {
        $this->create = $create;

        return $this;
    }

    /**
     * Get create
     *
     * @return \DateTime
     */
    public function getCreate()
    {
        return $this->create;
    }

    /**
     * Set update
     *
     * @param \DateTime $update
     *
     * @return BookFlight
     */
    public function setUpdate($update)
    {
        $this->update = $update;

        return $this;
    }

    /**
     * Get update
     *
     * @return \DateTime
     */
    public function getUpdate()
    {
        return $this->update;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return BookFlight
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set flightId
     *
     * @param integer $flightId
     *
     * @return BookFlight
     */
    public function setFlightId($flightId)
    {
        $this->flightId = $flightId;

        return $this;
    }

    /**
     * Get flightId
     *
     * @return integer
     */
    public function getFlightId()
    {
        return $this->flightId;
    }

    /**
     * Set passengerNumber
     *
     * @param integer $passengerNumber
     *
     * @return BookFlight
     */
    public function setPassengerNumber($passengerNumber)
    {
        $this->passengerNumber = $passengerNumber;

        return $this;
    }

    /**
     * Get passengerNumber
     *
     * @return integer
     */
    public function getPassengerNumber()
    {
        return $this->passengerNumber;
    }

    /**
     * Set paid
     *
     * @param boolean $paid
     *
     * @return BookFlight
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return boolean
     */
    public function getPaid()
    {
        return $this->paid;
    }
}

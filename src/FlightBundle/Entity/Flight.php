<?php


namespace FlightBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 *
 * @ORM\Table(name="un_travel_flight", indexes={
 * @ORM\Index(name="date_origin_destination",
 *  columns={"date_departure_ymd", "origin_airport_id", "destination_airport_id"}),
 * @ORM\Index(name="airline", columns={"airline_id"}),
 * @ORM\Index(name="availability", columns={"availability"}),
 *                      })
 * @ORM\Entity(repositoryClass="FlightBundle\Repository\FlightRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Flight {

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    protected $updated;

    /**
     * @ORM\Column(type="datetimetz")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    protected $dateDeparture;

    /**
     * Integer date by date(Ymd)
     *
     * @ORM\Column(type="integer", length=8)
     */
    protected $dateDepartureYmd;

    /**
     * @ORM\Column(type="datetimetz")
     */
    protected $dateArrival;

    /**
     *
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="Airport", inversedBy="origin")
     * @ORM\JoinColumn(name="origin_airport_id", referencedColumnName="id")
     *
     */
    protected $originAirport;

    /**
     *
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="Airport", inversedBy="destination")
     * @ORM\JoinColumn(name="destination_airport_id", referencedColumnName="id")
     *
     */
    protected $destinationAirport;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="Airline", inversedBy="flights")
     * @ORM\JoinColumn(name="airline_id", referencedColumnName="id")
     *
     */
    protected $flightAirline;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="Aircraft", inversedBy="flights")
     * @ORM\JoinColumn(name="aircraft_id", referencedColumnName="id")
     *
     */
    protected $flightAircraft;


    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Length(max=3)
     */
    protected $availability;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank()
     * @Assert\Length(min=4)
     */
    protected $flightNumber;


    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     */
    protected $price;


   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Flight
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Flight
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set dateDeparture
     *
     * @param \DateTime $dateDeparture
     *
     * @return Flight
     */
    public function setDateDeparture($dateDeparture)
    {
        $this->dateDeparture = $dateDeparture;

        return $this;
    }

    /**
     * Get dateDeparture
     *
     * @return \DateTime
     */
    public function getDateDeparture()
    {
        return $this->dateDeparture;
    }

    /**
     * Set dateDepartureYmd
     *
     * @param integer $dateDepartureYmd
     *
     * @return Flight
     */
    public function setDateDepartureYmd($dateDepartureYmd)
    {
        $this->dateDepartureYmd = $dateDepartureYmd;

        return $this;
    }

    /**
     * Get dateDepartureYmd
     *
     * @return integer
     */
    public function getDateDepartureYmd()
    {
        return $this->dateDepartureYmd;
    }

    /**
     * Set dateArrival
     *
     * @param \DateTime $dateArrival
     *
     * @return Flight
     */
    public function setDateArrival($dateArrival)
    {
        $this->dateArrival = $dateArrival;

        return $this;
    }

    /**
     * Get dateArrival
     *
     * @return \DateTime
     */
    public function getDateArrival()
    {
        return $this->dateArrival;
    }

    /**
     * Set availability
     *
     * @param integer $availability
     *
     * @return Flight
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return integer
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Set flightNumber
     *
     * @param string $flightNumber
     *
     * @return Flight
     */
    public function setFlightNumber($flightNumber)
    {
        $this->flightNumber = $flightNumber;

        return $this;
    }

    /**
     * Get flightNumber
     *
     * @return string
     */
    public function getFlightNumber()
    {
        return $this->flightNumber;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Flight
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set originAirport
     *
     * @param \FlightBundle\Entity\Airport $originAirport
     *
     * @return Flight
     */
    public function setOriginAirport( $originAirport = null)
    {
        $this->originAirport = $originAirport;

        return $this;
    }

    /**
     * Get originAirport
     *
     * @return \FlightBundle\Entity\Airport
     */
    public function getOriginAirport()
    {
        return $this->originAirport;
    }

    /**
     * Set destinationAirport
     *
     * @param \FlightBundle\Entity\Airport $destinationAirport
     *
     * @return Flight
     */
    public function setDestinationAirport( $destinationAirport = null)
    {
        $this->destinationAirport = $destinationAirport;

        return $this;
    }

    /**
     * Get destinationAirport
     *
     * @return \FlightBundle\Entity\Airport
     */
    public function getDestinationAirport()
    {
        return $this->destinationAirport;
    }

    /**
     * Set flightAirline
     *
     * @param \FlightBundle\Entity\Airline $flightAirline
     *
     * @return Flight
     */
    public function setFlightAirline( $flightAirline = null)
    {
        $this->flightAirline = $flightAirline;

        return $this;
    }

    /**
     * Get flightAirline
     *
     * @return \FlightBundle\Entity\Airline
     */
    public function getFlightAirline()
    {
        return $this->flightAirline;
    }

    /**
     * Set flightAircraft
     *
     * @param \FlightBundle\Entity\Aircraft $flightAircraft
     *
     * @return Flight
     */
    public function setFlightAircraft( $flightAircraft = null)
    {
        $this->flightAircraft = $flightAircraft;

        return $this;
    }

    /**
     * Get flightAircraft
     *
     * @return \FlightBundle\Entity\Aircraft
     */
    public function getFlightAircraft()
    {
        return $this->flightAircraft;
    }

    public function getTravelTime()
    {

        $interval = $this->getDateDeparture()->diff($this->getDateArrival());

        return  $interval->format('%h hours');
    }
}

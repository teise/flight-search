<?php

namespace FlightBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotBlank;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="search_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $options["airports"] = \FlightBundle\User\Helper::prepareAirport(
            $em->getRepository('FlightBundle:Airport')->findAll()
        );

        $options["airlines"] = array_merge([" " => 0],\FlightBundle\User\Helper::prepareAirport(
            $em->getRepository('FlightBundle:Airline')->findAll())
        );

        $form = $this->createForm('FlightBundle\Form\FlightSearch', null, $options);

        $form->handleRequest($request);

        $flights = null;
        $flightsReturn = null;

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $arParams = [];

            if ($data['flightAirline'] > 0) {
                $arParams['airline'] = $data['flightAirline'];
            }

            $arParams = array_merge(
                $arParams,
                [
                    'fdate' => $data['date']->format('Ymd'),
                    'origin' => $data['originAirport'],
                    'destination' => $data['destinationAirport']
                ]
            );

            $arParamsReturn = array_merge(
                $arParams,
                [
                    'fdate' => $data['dateReturn']->format('Ymd'),
                    'origin' => $data['destinationAirport'],
                    'destination' => $data['originAirport']
                ]
            );

            // There are two query because complex_index (date_origin_destination) is not work with OR
            $em = $this->getDoctrine()->getEntityManager();
            $flights = $em->getRepository('FlightBundle:Flight')->filterFlightByParams($arParams);

            $em = $this->getDoctrine()->getEntityManager();
            $flightsReturn = $em->getRepository('FlightBundle:Flight')->filterFlightByParams($arParamsReturn);
        }


        return $this->render(
            'default/index.html.twig',
            [
                'form' => $form->createView(),
                'flights' => $flights,
                'flightsReturn' => $flightsReturn
            ]
        );
    }
}


if (isset($data['requested_site'])) {

    $requestedSite = $data['requested_site'];
    $action = null;
    $requireActionInfo = array('email_only_reg', 'email_pass_reg');

    //currently we only want to append action for "email_only_reg" and "email_pass_reg".
    if (isset($data['login_history_key'])
        && in_array($data['login_history_key'], $requireActionInfo)) {

        $action = $data['login_history_key'] . '-' . $requestedSite;

    // Anonymous login history for mobile apps [iphoneapp, androidapp]
    } elseif (
        isset($data['registrationType'])
        && $data['registrationType'] === 'anonymous'
        && isset($deviceType)
        && in_array($deviceType, array('iphoneapp', 'androidapp'))) {

        $action = 'email_pass_reg_' . $requestedSite;

    } else {
        $action = 'register_and_login-' . $requestedSite;
    }
}




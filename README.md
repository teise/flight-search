Symfony Flight Search
========================

Test task, flight search and manage's flights

Unit test, form of order and authorization were not finished

cache via default symfony

Frontend bootstrap

installation
--------------

  * Install symfony via composer   (composer install);

  * Generate test data dump  - mysql -u user -p test_flight_search.sql

  * Set config parameters cp app/config/parameters.xml.dist app/config/parameters.xml

  * dev web server bin/console server:run

  * It should work

What is inside
---------------

src/FlightBundle/Entity - entities

  * Flight is the main entity

  * Aircraft, Airline, Airport  are directory

src/FlightBundle/Repository 

  * consists of user query function

src/FlightBundle/User 

  * store for helpfull function

app/Resources/views 

  * consists of views (Twig)

/src/FlightBundle/Controller

  *  DefaulController for flight search

  *  FlightController for flight's manager

/web/css

  * css

bootstrap include by CDN

Functionality
-------------
 
  *  /flight/ - is flight's manager

  *  /        - flight search